const express = require("express");
const bodyParser = require('body-parser');

const app = express();
const port = 3000;

const { Mintblue, utils } = require("@mintblue/sdk");

const sdkToken = "<YOUR-SDK-TOKEN>";
let projectId = "<YOUR-PROJECT-ID>";
let client;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.post("/", async (req, res) => {
  // calculate data hash
  const byteArray = await utils.sha256(req.body.data);
  let hash = Buffer.from(byteArray).toString("base64");

  // look up hash in notary
  let result = await findNotaryRecord(hash);
  if (result) {
    // notary record found, show result
    res.send(
      `Found notary record:<br />Transaction ID: ${result.txid}<br />SHA256: ${result.hash}<br />Timestamp: ${result.timestamp}`
    );
  } else {
    // no notary record found, create new record and show
    const { txid } = await client.createTransaction({
      project_id: projectId,
      outputs: [
        {
          type: "data",
          value: hash,
        },
      ],
    });
    const result = await findNotaryRecord(hash);
    res.send(
      `Created notary record:<br />Transaction ID: ${result.txid}<br />SHA256: ${result.hash}<br />Timestamp: ${result.timestamp}`
    );
  }
});

app.get("/", (req, res, next) => {
  // present simple form allowing the user to input a string
  res.send(`
    <form method="POST" action="/">
        <input type="text" name="data" placeholder="data">
        <input type="submit">
    </form>
  `);
});

const getNotary = async () => {
  // get notary transactions
  const txs = await client.listTransactions({ project_id: projectId });
  const notary = [];
  for (tx of txs) {
    // fetch notary record payload
    const payload = await client.getTransaction({
        txid: tx.txid,
        parse: true,
    });

    notary.push({
      txid: tx.txid,
      hash: payload.outputs[0].value,
      timestamp: tx.published_at,
    });
  }

  return notary;
};

// find hash in notary records
const findNotaryRecord = async (hash) => {
  const hashes = await getNotary();

  const result = hashes.find((el) => {
    return el.hash === hash;
  });
  return result;
};

// set up express listener
app.listen(port, async () => {
  client = await Mintblue.create({ token: sdkToken });
  console.log(`Notary app listening on port ${port}`);
});