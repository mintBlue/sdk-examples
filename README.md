# SDK Examples

This repository contains two examples that make use of the mintBlue SDK. 

## Installation

To install the required dependencies, run the following command:

```sh
npm install
```

Make sure to replace the `<YOUR-PROJECT-ID>` and `<YOUR-SDK-TOKEN>` placeholders in both projects with your own project ID and SDK token.

## Example 1: Password Manager

[🔗 Docs](https://docs.mintblue.com/example-applications/password-manager)

This example demonstrates how to use the mintBlue SDK to create a simple password manager. The password manager allows you to store and retrieve passwords for different websites.

To run the password manager, execute the following command:

```sh
cd password-manager
node manager.js
```

## Example 2: Notary Express App

[🔗 Docs](https://docs.mintblue.com/example-applications/notary-express-app)

This example demonstrates how to use the mintBlue SDK to create a notary express app. The notary express app allows you to notarize documents.

To run the notary express app, execute the following command:

```sh
cd notary
node index.js
```

Next, navigate to `http://localhost:3000` in your web browser to access the notary express app.
