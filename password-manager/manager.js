const { Mintblue } = require("@mintblue/sdk");

// show the user how to use the app when they don't provide the right arguments
if (process.argv.length < 3 || !["get", "add"].includes(process.argv[2])) {
  console.log("Usage:");
  console.log(
    "- Add/Update password: node manager.js add [account] [password]"
  );
  console.log("- Retrieve password: node manager.js get [account]");
  process.exit();
}

const command = process.argv[2];
const account = process.argv[3];
let password = process.argv[4];

async function main() {
  const sdkToken = "<YOUR-SDK-TOKEN>";
  let client = await Mintblue.create({ token: sdkToken });
  let projectId = "<YOUR-PROJECT-ID>";

  if (command === "add") {
    // store account and password encrypted on blockchain
    const { txid } = await client.createTransaction({
      project_id: projectId,
      outputs: [
        {
          type: "data",
          value: { account, password },
          encrypt: true,
        },
      ],
    });
    console.log(
      `Password stored encrypted in transaction: https://whatsonchain.com/tx/${txid}`
    );
  }

  if (command === "get") {
    // get all of this project's transactions
    const transactions = await client.listTransactions({
      project_id: projectId,
    });

    // find password record for requested account and decrypt it
    for (let i = 0; i < transactions.length; i++) {
      const txn = await client.getTransaction({
        txid: transactions[i].txid,
        parse: true, // default is true
      });

      // Show decrypted password to user when found
      if (txn.outputs[0].value.account === account) {
        console.log(
          `Password for ${account} is: ${txn.outputs[0].value.password}`
        );
        process.exit();
      }
    }
    console.log(`Password not found for: ${account}`);
  }
}

main();
